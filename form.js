const typeResolver = require('./types/type_resolver');

/**
 * Simple class that contains errors and provide some useful methods for check errors and obtain errors for some type
 *
 * @author Vyacheslav Gusser vgusser@wrum.me
 */
class ValidationResult {
  constructor(errors) {
    this.errors = errors;
  }

  hasErrors() {
    for (let err of Object.values(this.errors)) {
      if (err.length) {
        return true;
      }
    }
    return false;
  }
}

const resolveTypes = (config, model) => {
  const _resolveType = type => {
    const typeClass = typeResolver(type);

    if (!typeClass) {
      throw new Error(`Type '${type}' not resolved`)
    }

    return typeClass;
  }

  return config.map(it => {
    const {type, name} = it;

    const Type = _resolveType(type);

    return new Type(it, model[name || type])
  })
}

/**
 * This form class that provide methods for schema validation.
 *
 * @author Vyacheslav Gusser vgusser@wrum.me
 */
class Form {
  constructor(form, model) {
    this.form = form;
    this.model = model;
  }

  static factory(config, model) {
    if (!Array.isArray(config)) {
      throw new Error('Form config must be array')
    }

    if (!model || typeof model !== 'object') {
      throw new Error('Model must be set and must be object!')
    }

    return new Form(resolveTypes(config, model), model);
  }

  /**
   * This run validation of the form.
   *
   * This parse form config, resolve types and run validation for given model.
   *
   * @returns {number}
   */
  validate() {
    const errors = this.form.map(it => {
      return {
        [it.fieldName]: it.validate() || []
      }
    }).reduce((a, b) => ({...a, ...b}))

    return new ValidationResult(errors)
  }

  /**
   * This normalize source model by resolved types. This required in some cases. For example, when client application
   * pass price with spaces, we should remove all spaces and return clear number.
   */
  normalizeFormDataWithTypes() {
    const copy = Object.assign({}, this.model)

    this.form.forEach(type => {
      copy[type.fieldName] = type.clearValue
    })

    return copy
  }

  /**
   * This common useful method which return promise that will be resolved with normalized data if form is valid,
   * otherwise promise will be rejected with validation errors
   */
  promisedFlow() {
    return new Promise((resolve, reject) => {
      const errors = this.validate()

      if (errors.hasErrors()) {
        reject(errors.errors)
      } else {
        resolve(this.normalizeFormDataWithTypes())
      }
    })
  }
}

module.exports = Form;
