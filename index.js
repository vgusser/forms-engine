const formConfig = require('./form.json');
const request = require('./request.json');
const Form = require('./form');

Form.factory(formConfig, request).promisedFlow().then(data => {
  console.log('form valid and can be persisted', data)
}).catch(errors => {
  console.log('form is invalid and can not be persisted', errors)
})
