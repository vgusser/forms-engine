const Type = require('./type')

/**
 * Another common type that uses in application. This have different name, but always resolve this type. For example in
 * schema we can have follow types that always be resolved to combobox:
 *
 * <pre>
 * [
 *  {
 *    "type": "combobox",
 *    "name": "size",
 *    "values": [
 *      "S",
 *      "M",
 *      "XL",
 *      "XXL"
 *    ]
 *  },
 *  {
 *    "type": "radio",
 *    "name": "condition",
 *    "values": [
 *      {
 *        "name": "Б/У",
 *        "value": "USED"
 *      },
 *      {
 *        "name": "Новое",
 *        "value": "NEW"
 *      }
 *    ]
 *  }
 * ]
 * </pre>
 *
 * @author Vyacheslav Gusser vgusser@wrum.me
 */
class Combobox extends Type {
  constructor(config, value) {
    super(config, value)
  }

  typeValidators() {
    return [
      {
        name: 'in',
        args: {
          allowed: this.config.values.map(v => (
            typeof v === 'object' ? v.value : v
          ))
        }
      }
    ];
  }
}

module.exports = Combobox;