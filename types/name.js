const Type = require('./type')

/**
 * Common type that required for the majority products, this is name of product. We have shortcut definition for this
 * type in schema. Like below
 *
 * <pre>
 * {
 *   "type": "name"
 * }
 * </pre>
 *
 * @author Vyacheslav Gusser vgusser@wrum.me
 */
class Name extends Type {
  constructor(config, value) {
    super(config, value)
  }

  typeValidators() {
    return [{
      name: 'required'
    }]
  }
}

module.exports = Name;