module.exports = {

  /**
   * Simple "required" validator that check that value present, if value does not present, this return errors
   */
  required(value) {
    if (!value) {
      return 'Обязательное поле'
    }
  },

  /**
   * Validator that checks that given value present in allowed values list
   */
  in(value, {allowed}) {
    if (!allowed || !Array.isArray(allowed)) {
      throw new Error('Allowed is required!')
    }

    const el = allowed.find(el => el === value)

    if (!el) {
      return `Значение '${value}' не допустимо`
    }
  }
}