const Type = require('./type')

/**
 * This is also common value like Name, in all products this is required for all products. For use it in schema write
 * config like below
 *
 * <pre>
 * {
 *   "type":"price"
 * }
 * </pre>
 *
 * @author Vyacheslav Gusser vgusser@wrum.me
 */
class Price extends Type {
  constructor(config, value) {
    super(config, value);
  }

  typeValidators() {
    return [{
      name: 'required'
    }]
  }

  /**
   * This should have implementation for #clearValue, because frontend app may pass price with spaces and another
   * symbols, but in app we should save it in raw format, like simple number!
   *
   * @returns {void | string | *}
   */
  get clearValue() {
    return this.value.replace(/\D/g, '')
  }
}

module.exports = Price;