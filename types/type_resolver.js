const Name = require('./name');
const Combobox = require('./combobox')
const Price = require('./price')
const Type = require('./type')

module.exports = type => {
  switch (type) {
    case 'name':
      return Name;
    case 'description':
      // non unique field
      return Type;
    case 'price':
      return Price;
    case 'radio':
    case 'combobox':
      return Combobox;
  }
};
