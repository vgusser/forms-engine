const validators = require('./validators/validators')

/**
 * Form type, this should be used when another type can't be resolved.
 *
 * @author Vyacheslav Gusser vgusser@wrum.me
 */
class Type {

  /**
   * Primary constructor
   *
   * @param config configuration for current type
   * @param value that assigned to type
   */
  constructor(config, value) {
    this.value = value
    this.config = config
  }

  /**
   * This should return array of objects with validators that hard code required for type
   * @returns {*[]}
   */
  typeValidators() {
    return []
  }

  /**
   * This run validations on type that assigned in configuration
   *
   * @returns {*[]}
   */
  validate() {
    const {rules = []} = this.config

    return [...rules, ...this.typeValidators()].map(rule => {
      const {name, args} = rule

      if (!validators[name]) {
        throw new Error(`Unknown validator '${name}'`)
      }

      return validators[name](this.value, args)
    }).filter(v => !!v) || []
  }

  /**
   * This return field name that assigned to current type
   *
   * @returns {*}
   */
  get fieldName() {
    return this.config.name || this.config.type;
  }

  /**
   * In common scenarios this always return raw value without any modifications but in some cases this may clear values
   * or apply any modifications
   */
  get clearValue() {
    return this.value
  }
}

module.exports = Type;